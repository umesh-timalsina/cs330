/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chapter2Implementations;

/**
 *
 * @author Evaluate the value of a polynomial
 */
public class PolynomialEvaluation {
    public static double valueOf(double [] coeff, double x){
        if(coeff.length < 1 ) throw new IllegalArgumentException(
                "Atleast one Coefficient is necessary");
        double y = 1;
        double value = coeff[0];
        for(int i = 1; i < coeff.length; i++){
            y *= x;
            value+=(y*coeff[i]);
        }
        return value;
    }
    
    // Requires less multiplications than the method: valueOf
    public static double hornerValueOf(double [] coeff, double x){
        double value = coeff[coeff.length-1];
        for(int i = coeff.length-2; i>=0; i--){
            value = value * x;
            value+=coeff[i];
        }
        return value;
    }// Calculated using horners rule : c_1 x^2+c_2 x^1 + c_3 = (c_1 * x + c_2) *x + c_3
    
    public static void main(String[] args) {
        System.out.println(valueOf(new double [] {1,4,1}, 2));
        System.out.println(hornerValueOf(new double [] {1,4,1}, 2));
    }

}
