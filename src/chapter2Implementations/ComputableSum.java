/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chapter2Implementations;

/**
 *
 * @author robotics
 */
public class ComputableSum {
    
    public static Integer recursiveSum(int [] a, int n){
        if(a.length>0){
            return rSum(a, n);
        }
        else return null;
    }// Driver for true recursive method rSum
    public static Integer rSum(int [] a, int n){
        if (n==0) return 0;
        else{ 
            System.out.println("The element added was" + a[n-1]);
            return rSum(a, n-1) + a[n-1];
        }
        
    }// A recursive method to find the sum of an array.
    public static void main(String[] args) {
      int [] a = {1,2,3,4,5,7                                                                                                                                  };
        System.out.println(recursiveSum(a, a.length));
    }
}
