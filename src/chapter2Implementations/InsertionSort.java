/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chapter2Implementations;

/**
 *
 * @author robotics
 */
public class InsertionSort {
    public static void insert(int [] a, int n, int x){
        int i;
        for(i = n-1; i >= 0 && x < a[i]; i--){
            a[i+1] = a[i];
        }
        a[i+1] = x; // insert x next.
    }
    
    private static void printArray(int [] anArray) {
      for (int i = 0; i < anArray.length; i++) {
         if (i > 0) {
            System.out.print(", ");
         }
         System.out.print(anArray[i]);
      }
        System.out.println("");
    }
    
    public static void main(String [] args){
        int arr [] = new int [5];
        insert(arr, 4, 2);
        insertionSort(arr);
        printArray(arr);
    }
    
    public static void insertionSort(int [] arr){
        for(int i = 1; i < arr.length; i++){
            insert(arr, i, arr[i]);
        }
    }

}
