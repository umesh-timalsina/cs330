/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chapter2Implementations;

/**
 *
 * @author robotics
 */
public class Permutations {
    public static void perm(Object [] list, int k, int m){
        int i;
        if(k == m){
            for(i=0; i <= m; i++){
                System.out.print(list[i]);
            }
            System.out.println("");
        }
        else{
            for(i = k; i <= m; i++){
                swap(list, k, i);
                perm(list, k+1, m);
                swap(list, k, i);
            }
        }
    }
    
    public static void swap(Object [] list, int k, int i){
        Object tmp = list[k];
        list[k] = list[i];
        list[i] = tmp;
    }
    
    private static void printArray(Object[] anArray) {
      for (int i = 0; i < anArray.length; i++) {
         if (i > 0) {
            System.out.print(", ");
         }
         System.out.print(anArray[i]);
      }
        System.out.println("");
   }
    public static void main(String[] args) {
          perm(new Object[] {"A ", "B ", "C "}, 0, 2);
//        Object [] arr = new Object[] {"Appple ", "Ball ", "Cat "};
//        swap(arr, 2,0);
//        printArray(arr);
//        swap(arr, 2,0);
//        printArray(arr);
    }

}
