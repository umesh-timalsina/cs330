/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chapter2Implementations;

/**
 *
 * @author robotics
 */
public class BubbleSort {
    public static void bubble(int [] arr, int n){
        for(int i = 0; i < n-1; i++){
            if(arr[i] > arr[i+1]){
                swap(arr, i, i+1);
                
            }
        }
    }
    
    private static void printArray(int [] anArray) {
      for (int i = 0; i < anArray.length; i++) {
         if (i > 0) {
            System.out.print(", ");
         }
         System.out.print(anArray[i]);
      }
        System.out.println("");
    }
    
    public static void swap(int [] list, int k, int i){
        int tmp = list[k];
        list[k] = list[i];
        list[i] = tmp;
    }
    
    public static void main(String[] args) {
        int [] arr = {4,7,1,3,5};
        bubble(arr, arr.length);
        printArray(arr);
    }
}
