/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chapter2Implementations;

/**
 *
 * @author robotics
 */
public class SequentialSearch {
    @SuppressWarnings("empty-statement")
    public static int sequentialSearch(Object [] a, Object x){
        int i;
        for(i = 0; i < a.length && !x.equals(a[i]); i++);
            if(i == a.length) return -1;
            else return i;
    }// The space requirements for sequential search is zero
    
    
    public static void main(String[] args) {
        String [] a = {"A", "P", "P", "L", "E"};
        System.out.println(sequentialSearch(a, "P"));
    }
}
