/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chapter2Implementations;

/**
 *
 * @author robotics
 */
public class RankSort {
    public static void rank(int [] a, int [] r){
        if(r.length != a.length){
            throw new IllegalArgumentException("Both must have same lengths");
        }
        
        for(int i = 0; i < r.length; i++){
            r[i] = 0;
        }
        
        for(int i = 1; i < a.length; i++){
            for(int j = 0; j < i; j++){
                if(a[i]>=a[j]) r[i]++;
                else r[j]++;
            }
        }
    }// The number of comparisons in this case is n(n-1)/2.
    
    public static void rearrange(int [] a, int [] r){
        int [] u = new int [a.length];
        for(int i = 0; i<a.length; i++){
            u[r[i]] = a[i];
        }
        System.arraycopy(u, 0, a, 0, u.length);
    }// The number of element reference moves in this case is 2n.
    
    public static void inPlaceRearrange(int [] a, int [] r){
        for(int i = 0; i < a.length; i++){
            while(r[i] != i){
                int t = r[i];
                swap(a, i, t);
                swap(r, i, t);
            }
        }
    } //ingenuous really ingenuous
    
    public static void swap(int [] list, int k, int i){
        int tmp = list[k];
        list[k] = list[i];
        list[i] = tmp;
    }
        
    public static void printArray(int [] anArray) {
      for (int i = 0; i < anArray.length; i++) {
         if (i > 0) {
            System.out.print(", ");
         }
         System.out.print(anArray[i]);
      }
        System.out.println("");
    }
    
    
   
    public static void main(String[] args) {
        
        int [] a = new int [] {4,3,9,3,7};
        int [] r = new int [] {0,0,0,0,0}; 
        rank(a,r);
        printArray(a);
        rearrange(a, r);
        printArray(r);
        inPlaceRearrange(a, r);
        printArray(r);
        printArray(a);
    }
}
