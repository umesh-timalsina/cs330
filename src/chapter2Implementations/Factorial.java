/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chapter2Implementations;

/**
 *
 * @author robotics
 */
public class Factorial {

    public static int factorial(int n){
        if(n <= 1){
            return 1;
        }
        else return n * factorial(n-1);
    }// the recursion depth is max{1,n}
    public static void main(String[] args) {
        System.out.println(factorial(6));
    }
}
