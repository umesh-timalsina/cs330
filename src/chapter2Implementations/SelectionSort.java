/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chapter2Implementations;

/**
 *
 * @author robotics
 */
public class SelectionSort {
    // A quick try of selection sort
    public static void selSort(int [] arr){
        for(int i = 0; i < arr.length; i++){
            int j = max(arr, (arr.length-1)-i);
            swap(arr,j,(arr.length-1)-i);
        }
    }
    public static int max(int [] arr, int n){
        int positionOfMax = 0;
        for(int i = 0; i < n; i++){
            if(arr[positionOfMax] < arr[i]) positionOfMax = i;
        }
        return positionOfMax;
    }
    
    public static void swap(int [] list, int k, int i){
        int tmp = list[k];
        list[k] = list[i];
        list[i] = tmp;
    }
    
    private static void printArray(int [] anArray) {
      for (int i = 0; i < anArray.length; i++) {
         if (i > 0) {
            System.out.print(", ");
         }
         System.out.print(anArray[i]);
      }
        System.out.println("");
    }
    
    public static void etSelectionSort(int [] arr){
        // Early Terminating Version of Selection Sort
        boolean sorted = false;
        for(int size = arr.length; !sorted && size > 1; size--){
            int pos = 0;
            sorted = true;
            for(int i = 1; i < size ; i++){
                if(arr[pos] <= arr[i]) pos = i;
                else sorted = false;
                swap(arr, pos, size-1);
            }
        }
    }
    public static void main(String[] args) {
        int [] arr = {4,3,9,3,7};
        etSelectionSort(arr);
        printArray(arr);
    }
     
}
