/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linkedDictionary;

/**
 *
 * @author robotics
 * @param <T>
 */
public class SortedChainDictionary<T extends Comparable> implements Dictionary<T> {
    private Node firstNode;
    private Node currentNode;
    private int numberOfEntries;

    @Override
    public T get(T theKey){
        this.currentNode = this.firstNode;
        while(currentNode != null && currentNode.getKey().compareTo(theKey) < 0){
            // iterate till will find the key.
            System.out.println("Comparing " + 
                    currentNode.getKey() + " With " + theKey  );
            currentNode = currentNode.getNextNode();
            
        }
        if(currentNode!=null && currentNode.getKey().equals(theKey))    
            // if match is found return match
            return (T) currentNode.getData();
        return null;
    } // Getting the value of the element with a specified key
    
    @Override
    public T put(T key, T theElement){
        this.currentNode = this.firstNode;
        Node pointedNode = null; // Used if no match is found
         // This implies the dictionary is empty at first
        while(this.currentNode != null && 
                this.currentNode.getKey().compareTo(key) < 0){
            pointedNode = currentNode;
            currentNode = currentNode.getNextNode();
        }
        if(this.currentNode != null && this.currentNode.getKey().equals(key)){
            T returnObject = (T)this.currentNode.getData();
            this.currentNode.setData(theElement);
            return returnObject;
        }
       
        Node newNode = new Node(key, theElement, this.currentNode);
        if (pointedNode == null) this.firstNode = newNode;
        else pointedNode.setNextNode(newNode);
        this.numberOfEntries++;
        return null;
    }// Puts a key value pair into the dictionary

    
    @Override
    public T remove(T key) {
       this.currentNode = this.firstNode;
       Node tp = null;
       while(this.currentNode!=null &&
               this.currentNode.getKey().compareTo(key) < 0 ){
           tp = this.currentNode;
           currentNode = currentNode.getNextNode();
       }
       T objectToReturn = null;
       if(this.currentNode != null && this.currentNode.getKey().equals(key)){
           objectToReturn = (T) currentNode.getData();
           //System.out.println("Removed" + objectToReturn);
           if(tp == null) firstNode = currentNode.getNextNode();
           else tp.setNextNode(currentNode.getNextNode());
           this.numberOfEntries--;
       
       }
       return objectToReturn;       
    } // Remove a particular element
    
    public void printAllElements(){
       this.currentNode = this.firstNode;
       while(this.currentNode != null){
           System.out.println(this.currentNode.getKey() + " : "+ this.currentNode.getData());
           this.currentNode = currentNode.getNextNode();
       }
    
    }
    public int getCurrentSize(){
        return this.numberOfEntries;
    }// Returns the number of entries in the dictionary

    

}
