/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linkedDictionary;

/**
 *
 * @author robotics
 * @param <T> Any generic implementations
 */
public interface Dictionary<T extends Comparable> {
    public T get(T key);
    public T put(T key, T theElement);
    public T remove(T key);
}
