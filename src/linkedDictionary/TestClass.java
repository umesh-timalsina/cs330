/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linkedDictionary;

/**
 *
 * @author robotics
 */
public class TestClass {
    public static void main(String[] args) {
        SortedChainDictionary<String> dict1 = new SortedChainDictionary();
        dict1.put("Appple", "A fruit");
        dict1.put("ball", "A rounded element");
        dict1.put("Aeroplane", "A thing that flies");
        System.out.println(dict1.getCurrentSize());
        System.out.println(dict1.remove("Aeroplane"));
        dict1.printAllElements();
    }// main method
}
