/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package linkedDictionary;

/**
 *
 * @author robotics
 * @param <T>
 */
public class Node<T extends Comparable>  {
    private T data;
    private T key;
    private Node nextNode;
    
    // Constructor
    public Node(T key, T data){
        this(key, data, null);
    }
    
    // constructor with 2 parameters
    public Node(T key, T data, Node next){
        this.key = key;
        this.data = data;
        this.nextNode = next;
    }

    //** A series of getter and setter methods **\\
    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Node getNextNode() {
        return nextNode;
    }

    public T getKey() {
        return key;
    }

    public void setKey(T key) {
        this.key = key;
    }

    public void setNextNode(Node nextNode) {
        this.nextNode = nextNode;
}     
   
}
