/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homeWork3;

import java.util.Arrays;

/**
 *
 * @author robotics
 */
public class HashTableLinearProbing {
    private int divisor;
    private HashEntry [] table;
    private boolean [] neverUsed;
    private int currentSize;
    
    public HashTableLinearProbing(int size){
        this.divisor = size;
        this.table = new HashEntry[this.divisor];
        this.neverUsed = new boolean[this.divisor];
        Arrays.fill(neverUsed, true);
    }
    
    /* search for the index of a particular element */
    private int search(int theKey){
        int i = theKey%this.divisor; // the hash function
        int j = i;
        do{
            if(this.neverUsed[j] || 
                    (this.table[j] != null && this.table[j].entry == theKey))
                return j;
            j=(j+1)%this.divisor;
        }while(j != i);
        return j;
    } // if this loops gets executed, it means the table is full.
    
    public Integer put(int theKey){
        int b = this.search(theKey);
        if(this.table[b] == null){
            this.table[b]=new HashEntry(theKey);
            this.neverUsed[b] = false;
            this.currentSize++;
            return null;
        }// Fill the table at proper position.
        else{
            if(this.table[b].entry == theKey){
                int returnValue = this.table[b].entry;
//                this.table[b].entry = returnValue;
                return returnValue;
            }// Check for duplicate keys, same value reassignment not needed
            else // The hash table is full at this point
                throw new IllegalArgumentException("Table now full");
        }
    }// this returns an integer object, not the primitive int
    
    public Integer get(int theKey){
        int b = this.search(theKey);
        if(table[b] == null || !(this.table[b].entry == theKey)){
            return null;
        }
        else
            return this.table[b].entry;
    }// gives the value with specific entry
    
    public Integer remove(int theKey){
        int b = this.search(theKey);
        Integer returnValue = null;
        if(table[b] == null || !(this.table[b].entry == theKey)){
            return returnValue;
        }
        else{
            returnValue = this.table[b].entry;
            this.table[b] = null;
            this.currentSize--;
            return returnValue;
        }
    }
    
    public void printMe(){
        System.out.println("Index \t\t Value \t "
                + "     isNeverUsed");
        System.out.println("==================================="
                + "=========");
        for(int i = 0; i < this.divisor; i++){
            if(table[i] != null){
                System.out.print((i) +"\t\t  ");
                System.out.print(this.table[i].entry + "\t\t");
//                System.out.print(this.table[i].element + "\t\t");
                System.out.print(this.neverUsed[i]+"\t");
                System.out.println("");
            } // Print the index as well as the entry
            else{
                System.out.print((i) +"\t\t  ");
                System.out.print("null" + "\t\t");
//                System.out.print(this.table[i].element + "\t\t");
                System.out.print(this.neverUsed[i]+"\t");
                System.out.println("");
      
            }
        }
        System.out.println("==================================="
                + "=========");
    }

    private class HashEntry{
        public int entry;
        public HashEntry(int entry){
            this.entry = entry;
        }
    }// In this case key and value are the same
}
