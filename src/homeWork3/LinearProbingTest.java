/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homeWork3;

/**
 *
 * @author robotics
 */
public class LinearProbingTest {
    // Creating Hash Table of size 13.
    public static void main(String[] args) {
        HashTableLinearProbing htlp1 = new HashTableLinearProbing(13);
        int [] vals2Put = {9, 10, 22, 35, 11, 23, 2, 4, 15, 16, 48, 28};
        int [] vals2Rem = {22, 2, 11, 35, 28};
        for(int val : vals2Put){
            htlp1.put(val);
        } 
        htlp1.printMe();
        /* Question One is comleted here */
        
        for(int val : vals2Rem){
            htlp1.remove(val);
        }
        htlp1.printMe();
        /* Question Two is completed here */
        System.out.println(htlp1.get(2));
        System.out.println(htlp1.get(4));
        System.out.println(htlp1.get(48));
        
    }
    
    
}
