/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package treesImplementation;

/**
 *
 * @author robotics
 */
public class LinkedBinaryTree<T> {
    private int count;
    public LinkedBinaryTreeNode<T> root;
    
    public LinkedBinaryTree(LinkedBinaryTreeNode rootNode){
        this.root = rootNode;
        this.count= 1+rootNode.numChildren();
    }
    
    //===============================================
    // Creates a binary Tree with pre-specified root.
    //===============================================
    public LinkedBinaryTree(T theElement){
        count = 1;
        this.root = new LinkedBinaryTreeNode(theElement);
    } // Constructor linked Binary Tree
    
    public LinkedBinaryTree(T theElement, 
                            LinkedBinaryTree<T> leftSubtree,
                            LinkedBinaryTree<T> rightSubtree){
        this.root = new LinkedBinaryTreeNode(theElement);
        count = 1;
        if(leftSubtree != null){
            count = count+leftSubtree.size();
            this.root.leftChild = leftSubtree.root; 
        }
        else
            root.leftChild = null;
        
        if(rightSubtree != null){
            count = count+rightSubtree.size();
            this.root.rightChild = rightSubtree.root;
        }
        else 
           root.rightChild = null;
    }
    
    //======================================================================
    // Removes the leftsubtree from this tree and returns it as a new tree
    //======================================================================
    public LinkedBinaryTree removeLeftSubtree(){
        LinkedBinaryTree subTree = null;
        if(root.leftChild != null){
            subTree = new LinkedBinaryTree(this.root.leftChild);
            count=(count - root.leftChild.numChildren()-1);
            this.root.leftChild = null;
        }
        return subTree;
    }
    //======================================================================
    // Removes the leftsubtree from this tree and returns it as a new tree
    //======================================================================
    public LinkedBinaryTree removeRightSubTree(){
        LinkedBinaryTree subTree = null;
        if(this.root.rightChild != null){
            subTree = new LinkedBinaryTree(this.root.rightChild);
            count=(count - root.rightChild.numChildren() -1);
            this.root.rightChild = null;
        }
        return subTree;
    } // Method removeRightSubTree
    
    //=======================================================================
    // Clear Everything out
    //=======================================================================
    public boolean removeAllElements(){
        this.count = 0;
        this.root = null;
        return true;
    }// Method Remove All Elements
    
    //=======================================================================
    // Check if the tree is empty
    //=======================================================================
    public boolean isEmpty(){
        return (count==0);
    }// Method isEmpty
    
    //=======================================================================
    // Method For PreOrder Traversal of the tree
    //=======================================================================
    public void preOrder(){
        thePreOrder(this.root);
    }// PreOrder Traversal of the tree
    
    private static void thePreOrder(LinkedBinaryTreeNode t){
        if(t!=null){
            System.out.println(t.element);
            thePreOrder(t.leftChild);
            thePreOrder(t.rightChild);
        }
    }
    
    
    
    //=====================================================
    // Returns the size of this tree
    //======================================================
    public int size(){
        return count;
    }
    
}
