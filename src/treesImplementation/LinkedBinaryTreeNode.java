/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package treesImplementation;

/**
 *
 * @author robotics
 */
public class LinkedBinaryTreeNode<T> {
    T element;
    LinkedBinaryTreeNode leftChild;
    LinkedBinaryTreeNode rightChild;
    
    public LinkedBinaryTreeNode(){}
    public LinkedBinaryTreeNode(T theElement){
        this(theElement, null, null);
    }
    public LinkedBinaryTreeNode(T theElement, 
                            LinkedBinaryTreeNode theleftChild,
                            LinkedBinaryTreeNode therightChild){
        this.element = theElement;
        this.leftChild = theleftChild;
        this.rightChild = therightChild;
    }
    
    public int numChildren(){
        int children=0;
        if(this.leftChild != null){
            children = 1 + leftChild.numChildren();
        }
        if(this.rightChild != null){
            children += 1+rightChild.numChildren();
        }
        return children;
    }
    
    
}   
