/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package treesImplementation;

/**
 *
 * @author robotics
 */
public class Test {
    public static void main(String[] args) {
        LinkedBinaryTree<Character> lbt1 = new LinkedBinaryTree("K");
        LinkedBinaryTree<Character> lbt2 = new LinkedBinaryTree("L");
        LinkedBinaryTree<Character> lbt3 = new LinkedBinaryTree("M",lbt1,lbt2);
        LinkedBinaryTree<Character> lbt4 = lbt3.removeLeftSubtree();
        lbt3.preOrder();
    }
    
}
