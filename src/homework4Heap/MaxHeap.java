/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework4Heap;
import java.lang.Comparable;

/** Max-heap implementation */
public class MaxHeap<T extends Comparable> {
    private T[] Heap;   // Pointer to the heap array
    private int size;   // Maximum size of the heap
    private int n;      // Number of things in heap
    public MaxHeap(T[] h, int num, int max){
        Heap = h;
        n = num; 
        size = max; 
        buildheap(); 
    }
    
    public void printHeap(){
       for(int i=0; i<this.n; i++)
            System.out.print(this.Heap[i] + "\t");
        System.out.println("");
    }
    /** Return current size of the heap */
    public int heapsize() { return n; }
    /** Is pos a leaf position? */
    public boolean isLeaf(int pos)
    { return (pos >= n/2) && (pos < n); }
    /** Return position for left child of pos */
    public int leftchild(int pos) {
        assert pos < n/2 : "Position has no left child";
        return 2*pos + 1;
    }
    /** Return position for right child of pos */
    public int rightchild(int pos) {
        assert pos < (n-1)/2 : "Position has no right child";
        return 2*pos + 2;
    }
    /** Return position for parent */
    public int parent(int pos) {
        assert pos > 0 : "Position has no parent";
        return (pos-1)/2;
    }
    /** Heapify contents of Heap */
    public void buildheap()
    { for (int i=n/2-1; i>=0; i--) siftdown(i); }
    /** Insert into heap */
    public void insert(T val) {
        assert n < size : "Heap is full";
        int curr = n++;
        Heap[curr] = val;
        // Start at end of heap
        // Now sift up until curr’s parent’s key > curr’s key
        while ((curr != 0) && (Heap[curr].compareTo(Heap[parent(curr)]) > 0)) {
        
            swap(Heap, curr, parent(curr));
        curr = parent(curr);
        }
    }
    /** Put element in its correct place */
    private void siftdown(int pos) {
        assert (pos >= 0) && (pos < n) : "Illegal heap position";
        while (!isLeaf(pos)) {
            int j = leftchild(pos);
            if ((j<(n-1)) && (Heap[j].compareTo(Heap[j+1]) < 0))
                j++; // j is now index of child with greater value
            if (Heap[pos].compareTo(Heap[j]) >= 0)
                return;
            
            swap(Heap, pos, j);
            pos = j; // Move down
        }
    }
    public T removemax() {
        // Remove maximum value
        assert n > 0 : "Removing from empty heap";
        
        swap(Heap, 0, --n); // Swap maximum with last value
        if (n != 0)
           // Not on last element
            siftdown(0);
           // Put new heap root val in correct place
        return Heap[n];
    }
    /** Remove element at specified position */
    public T remove(int pos) {
        assert (pos >= 0) && (pos < n) : "Illegal heap position";
        
        swap(Heap, pos, --n); // Swap with last value
        while (Heap[pos].compareTo(Heap[parent(pos)]) > 0) {
            
            swap(Heap, pos, parent(pos));
            pos = parent(pos);
        }
        if (n != 0) siftdown(pos);
        // Push down
        return Heap[n];
    }
    
    public void swap(T [] arr, int src, int dest){
        T temp = arr[src];
        arr[src] = arr[dest];
        arr[dest] = temp;
    }   
}