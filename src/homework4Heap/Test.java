/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework4Heap;

/**
 *
 * @author tumesh
 */
public class Test {
    public static void main(String[] args) {
        Integer [] arr = {3, 6, 5, 10, 4, 2, 9, 7, 1, 8}; // New Heap Array
        MaxHeap<Integer> h1 = new MaxHeap(arr, arr.length, 50); // Creating new 
                                                                // Heap
        System.out.println("The Heap at this point is");
        h1.printHeap();
        
        System.out.println("Removing the first element");
        h1.remove(1);
        System.out.println("The Heap at this point is");
        h1.printHeap();
        
        System.out.println("Removing the third element");
        h1.remove(3);
        System.out.println("The Heap at this point is");
        h1.printHeap();
        
        System.out.println("Removing the first element");
        h1.remove(5);
        System.out.println("The Heap at this point is");
        h1.printHeap();
        
                                                                             
    }
}
