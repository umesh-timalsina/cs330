/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hashingImplementation;

import java.util.Arrays;

/**
 *
 * @author tumesh
 *  */
public class HashTable {
    private int divisor;
    private HashEntry [] table;
    private boolean [] isNeverUsed; // fileds for neverUsed. Each bucket has one
    private int size;
    
    public HashTable(int theDivisor){
        this.divisor = theDivisor;
        this.size = 0;
        this.table = new HashEntry[this.divisor];
        this.isNeverUsed = new boolean[this.divisor];
        Arrays.fill(this.isNeverUsed, true); // initializing isNeverUsed to all true
    }
    
    private int search(Object theKey){
        int i = Math.abs(theKey.hashCode())%this.divisor; // Home Bucket
        //System.out.println("Hash Code : " + i); 
        int j = i;
        do{
            if(this.isNeverUsed[j] || 
                    (this.table[j] != null && this.table[j].key.equals(theKey))){                 
                //if(this.table[j] == null && !this.isNeverUsed[j]);
                return j;
            }// Flip around to get a null pointer exception
 
            j=((j+1)%this.divisor);
        }while(j != i);// If this loop completes, the table is full.
        //System.out.println("I returned " + j);
        return j; // the table is full
        
    }// search for the element 
    
    public Object get(Object theKey){
        int b = search(theKey);
        if(table[b] == null || !this.table[b].key.equals(theKey)){
            return null;
        }
        else return this.table[b].element;
    }// Get the element with specified key
    
    public Object put(Object theKey, Object theElement){
        int b = search(theKey);
        if(this.table[b] == null){
            // no matching element and table b is not full
            this.table[b] = new HashEntry(theKey, theElement);
            this.isNeverUsed[b] = false;
            this.size++;
            return null;
        }
        else{
            // Check if dublicate or table full
            if(this.table[b].key.equals(theKey)){
                Object elementToReturn = table[b].element;
                this.table[b].element = theElement;
                return elementToReturn;
            }
            else //table is full
                throw new IllegalArgumentException("Table is full");
        }
    }

    public Object remove(Object theKey){
        int b = this.search(theKey);
        System.out.println(b);
        Object elementToReturn = null;
        if(this.table[b] != null && this.table[b].key.equals(theKey)){
            elementToReturn = this.table[b].element;
            this.table[b] = null;
            size--;
            return elementToReturn;
        }// if the element is found remove it
        else{
            System.out.println("No Matching Element Found");
            return elementToReturn;
        }
        
    }
    
    public void printMe(){
        this.printKeys();
        System.out.println("Entry Number \t Key \t\t    Value \t "
                + "     isNeverUsed");
        System.out.println("========================================="
                + "=========================");
        for(int i = 0, j=0; i < this.divisor; i++){
            if(table[i] != null){
                System.out.print((++j) +"\t\t");
                System.out.print(this.table[i].key + "\t\t");
                System.out.print(this.table[i].element + "\t\t");
//                System.out.print(this.isNeverUsed[i]+"\t\t");
                System.out.println("");
            }
            
        }
        System.out.println("==================================="
                + "===============================");
    }
    private void printKeys(){
        for(HashEntry k : this.table){
            if(k != null)
                System.out.print(k.key == null?"null":k.key + "\t");
            else
                System.out.print("null\t");
        }
        System.out.println("");
    }
    private class HashEntry{
        public Object key;
        public Object element;
        //public boolean isNeverUsed = true;
        public HashEntry(Object keyIn, Object elementIn){
            this.key = keyIn;
            this.element = elementIn;
//            this.isNeverUsed = false;
        }
    }

}
